-- Just an example, supposed to be placed in /lua/custom/

local M = {}

-- make sure you maintain the structure of `core/default_config.lua` here,
-- example of changing theme:

M.plugins = require "custom.plugins"

M.ui = {
  theme = "rosepine",
  override_options = {
    statusline = {
      separator_style = "round",
      overriden_modules = function()
        return require "custom.abc"
      end,
    },
  },
}

M.mappings = require "custom.mappings"

return M
