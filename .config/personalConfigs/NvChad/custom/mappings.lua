local M = {}

M.general = {
  n = {
    -- Navigation
    ["h"] = {"<Left>", "Move Left", opts = { noremap = true}},
    ["n"] = {"<Right>", "Move Right", opts = { noremap = true}},
    ["t"] = {"<Down>", "Move Down", opts = { noremap = true}},
    ["c"] = {"<Up>", "Move Up", opts = { noremap = true}},

    -- Hightlight
    ["l"] = {"n", "Next highlight selection", opts = { noremap = true}},
    ["L"] = {"N", "Prev highlight selection", opts = { noremap = true}},
    ["@"] = {"<cmd> :noh <CR>", "Remove highlight", opts = { noremap = true}},
  },

  i = {
    ["<A-h>"] = {"<Left>", "Move Left", opts = { noremap = true}},
    ["<A-n>"] = {"<Right>", "Move Right", opts = { noremap = true}},
    ["<A-t>"] = {"<Down>", "Move Down", opts = { noremap = true}},
    ["<A-c>"] = {"<Up>", "Move Up", opts = { noremap = true}},
  },

  v = {
    ["h"] = {"<Left>", "Move Left", opts = { noremap = true}},
    ["n"] = {"<Right>", "Move Right", opts = { noremap = true}},
    ["t"] = {"<Down>", "Move Down", opts = { noremap = true}},
    ["c"] = {"<Up>", "Move Up", opts = { noremap = true}},
  },
}

return M
