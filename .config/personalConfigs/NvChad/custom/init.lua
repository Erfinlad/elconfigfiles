-- example file i.e lua/custom/init.lua
-- load your options globals, autocmds here or anything .__.
-- you can even override default options here (core/options.lua)
local g = vim.g

g.user_emmet_leader_key='<C-e>'

-- language dependancies
g.loaded_python3_provider=1
g.python3_host_prog= '/usr/bin/python' 
-- g.loaded_node_provider=1
