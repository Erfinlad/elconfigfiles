call plug#begin()

" Auto pairing brackets, parenthesis, braces etc
Plug 'LunarWatcher/auto-pairs'
" Status bar you see under
Plug 'itchyny/lightline.vim'
" Language pack
Plug 'sheerun/vim-polyglot'
" Integrated File Manager
Plug 'preservim/nerdtree'
" Color viewer or somethin
Plug 'RRethy/vim-hexokinase'
" Language Server
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
" Autocomplete plugin
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'yami-beta/asyncomplete-omni.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/asyncomplete-emmet.vim'
" Themes
Plug 'ghifarit53/tokyonight-vim'

call plug#end()

if has("syntax")
      syntax on
endif
set nocompatible
set number
set mouse=a
set incsearch
set hlsearch
set ls=2
set cursorline
set termguicolors
set clipboard=unnamedplus

filetype plugin on
filetype indent on

"=====***** Tabs *****====="
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

"=====***** Maps *****====="

map h <Left>
map t <Down>
map n <Right>
map c <Up>
imap <A-h> <Left>
imap <A-t> <Down>
imap <A-n> <Right>
imap <A-c>  <Up>

map @ :noh<CR>

map <C-f> :NERDTreeToggle<CR>
map <C-g> :NERDTreeClose<CR>
map! <C-g> :NERDTreeClose<CR>
map! <C-f> :NERDTreeToggle<CR> 



"=====***** Appearance *****====="
let g:lightline = {
          \ 'colorscheme': 'tokyonight',
      \ }
let g:user_emmet_leader_key='<C-E>'
let g:user_emmet_mode='a'

let g:tokyonight_style='night'


packadd! dracula
syntax enable
colorscheme tokyonight

"=====***** Hexokinase *****====="
map <C-c> :HexokinaseToggle <CR>
let g:Hexokinase_optInPatterns = 'full_hex, rgb, rgba, hsl, hsla'
let g:Hexokinase_highlighters = ['backgroundfull']
let g:Hexokinase_ftOptInPatterns = {
\    'css': 'full_hex,rgb,rgba,hsl,hsla,colour_names',
\    'html': 'full_hex,rgb,rgba,hsl,hsla,colour_names',
    \}
"=====***** Syntax/Autocomplete *****====="
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"
"***** OMNI
autocmd User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#omni#get_source_options({
\ 'name': 'omni',
\ 'allowlist': ['*'],
\ 'blocklist': ['c', 'cpp', 'html'],
\ 'completor': function('asyncomplete#sources#omni#completor'),
\ 'config': {
\   'show_source_kind': 1,
\ },
\ }))
"***** EMMET
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#emmet#get_source_options({
    \ 'name': 'emmet',
    \ 'whitelist': ['html'],
    \ 'completor': function('asyncomplete#sources#emmet#completor'),
    \ }))

"=====***** LSP SERVERS *****====="
"****** HTML
if executable('html-languageserver')
  au User lsp_setup call lsp#register_server({
    \ 'name': 'html-languageserver',
    \ 'cmd': {server_info->[&shell, &shellcmdflag, 'html-languageserver --stdio']},
    \ 'whitelist': ['html'],
  \ })
endif
let g:lsp_settings_filetype_html = ['html-languageserver']
"***** CSS
if executable('css-languageserver')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'css-languageserver',
        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'css-languageserver --stdio']},
        \ 'whitelist': ['css', 'less', 'sass'],
        \ })
endif
let g:lsp_settings_filetype_css  = ['css-languageserver']
"***** C/C++
if executable('clangd')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'clangd',
        \ 'cmd': {server_info->['clangd', '-background-index']},
        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
        \ })
endif
let g:lsp_settings_filetype_c    = ['clangd']
"***** BASH
if executable('bash-language-server')
  augroup LspBash
    autocmd!
    autocmd User lsp_setup call lsp#register_server({
          \ 'name': 'bash-language-server',
          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
          \ 'allowlist': ['sh'],
          \ })
  augroup END
endif
let g:lsp_settings_filetype_bash = ['bash-language-server']
