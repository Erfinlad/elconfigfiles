from libqtile.config import Key, Group
from libqtile.command import lazy
from .keys import mod, keys

groups = [Group(i) for i in [
    "", "", "", "", "", "", "", "", "",
]]

keys.extend ([
    Keys([mod], "ampersand", lazy.group[""].toscreen()),
    ])
