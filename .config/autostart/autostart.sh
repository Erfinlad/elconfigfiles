setxkbmap -layout us -variant dvp
feh --bg-scale $HOME/Wallpapers/one-punch-man/saitama.png &
pgrep picom || picom &
pgrep xbanish || xbanish -i all  -t 10 -s &
pgrep clipmenud || clipmenud &
xbacklight -set 10 &
amixer set Master 60 &
pgrep xfsettingsd || xfsettingsd --daemon &
#wal -R
