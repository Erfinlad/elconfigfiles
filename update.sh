#!/bin/bash

homeconfigDir=$HOME/.config
gitconfigDir=$HOME/git/user/elconfigfiles/.config
files="GIMP Thunar fish X11 alacritty autostart awesome bspwm btop kitty personalConfigs picom polybar qtile qterminal.org slstatus st"

updateFunc(){
  echo "Performing Update"
  for file in $files;do
    echo "Copying $file to $gitconfigDir"
    cp -a $homeconfigDir/$file $gitconfigDir
  done
  echo "Done"
}

gitFunc(){
  updateFunc

  git add . && git commit -m "Update" && git push
}

mainFunc(){
  gitFunc
}

mainFunc
