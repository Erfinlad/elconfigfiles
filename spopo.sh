#!/bin/bash

gitDir=$HOME/git/user/elconfigfiles
homeconfigDir=$HOME/.config
gitconfigDir=$gitDir/.config
oldhomeDir=$gitDir/oldhomeDir
files="GIMP Thunar X11 alacritty autostart awesome bspwm btop kitty personalConfigs picom polybar qtile slstatus st"

#Perform Backup of Home Config Directory if it exists
backupFunc(){
  if [[ -d $homeconfigDir ]];then
    if [[ -d $oldhomeDir ]]; then
      echo "Moving files in Home Config Directory to oldhomeDir"
      for file in $files; do
        echo "Transferring $file in oldgitDir"
        mv $homeconfigDir/$file $oldhomeDir
      done
      echo "Done"
    else
      echo "No oldhomeDir found"
      echo "Creating oldhomeDir now"
      mkdir $oldhomeDir
      echo "Done"
      echo "Moving files in Home Config Directory to oldhomeDir"
      for file in $files; do
        echo "Transferring $file in oldgitDir"
        mv $homeconfigDir/$file $oldhomeDir
      done
      echo "Done"
    fi
  fi
}
  
spopo(){
  echo "Performing Spopo"
  for file in $files;do
    echo "Transferring $file to $homeconfigDir"
    cp -a $gitconfigDir/$file $homeconfigDir
  done
  echo "Done"
}

mainFunc(){
 backupFunc
 spopo
}

mainFunc
